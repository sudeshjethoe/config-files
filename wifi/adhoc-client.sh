#!/bin/bash
# script to connect to ad-hoc network
# run as root!

# disable NetworkManager:
systemctl stop NetworkManager.service

# enable network:
# systemctl start network.service
ifconfig ra0 down

# set parameters for connection:
iwconfig ra0 mode Ad-Hoc essid onlinegangsters key s:pindapastakat

# start dhcp client on ra0:
dhclient ra0 -v
