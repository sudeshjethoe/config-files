#!/bin/bash
# script to start adhoc networking and NAT forwarding for connected clients
# must manually set IP on clients!
# set correct dns servers in client (google does not work at bergwijkdreef)
DEVICE='wlan2'
iwconfig wlan0 mode Ad-Hoc essid onlinegangsters key s:pindapastakat

ip addr add 192.168.0.1/24 brd + dev $DEVICE

ip link set $DEVICE up

iptables -t nat -A POSTROUTING -o p6p1 -s 192.168.0.0/8 -d 0/0 -j MASQUERADE
iptables -A FORWARD -t filter -o p6p1 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -t filter -i p6p1 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
#dnsmasq -i $DEVICE -F 192.168.0.2,192.168.0.9,255.255.255.0,12h
