# .bash_profile
# only executed once
# .bashrc every time new console is opened

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
PATH=$PATH:$HOME/.local/bin:$HOME/bin:/opt/zenoss/bin
export PATH
synclient tapbutton1=1

