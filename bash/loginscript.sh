#!/bin/bash -x
# Loginscript to fix synaptics stuff
synclient tapbutton1=1
synclient HorizTwoFingerScroll=1
synclient VertTwoFingerScroll=1
synclient HorizEdgeScroll=1
# sudo ifconfig usb0 down
# iwpriv ra0 set WirelessMode=7 fix by setting right parameter in /etc/Wireless/RT2860/RT2860STA.dat
# G/N only mode
#service wpa_supplicant restart
#dhclient ra0 -v
