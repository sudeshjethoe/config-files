# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
alias sl='ls'
alias l='ls'
#alias su='su -p'
alias rm='rm -i'
alias pip='pip-python'
alias s1='ssh office4'
alias s2='ssh office1'
alias sha='ssh hadoop'
#/home/svjethoe/bin/loginscript.sh

export EDITOR=/usr/bin/vim
#export GIT_DIR=/home/svjethoe/.git

cd ~

# http://fedoramagazine.org/add-power-terminal-powerline/
if [ -f `which powerline-daemon` ]; then
    powerline-daemon -q
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
    . /usr/share/powerline/bash/powerline.sh
fi
