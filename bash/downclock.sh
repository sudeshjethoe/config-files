#!/bin/bash

# downclock to make laptop more stable
# http://en.altlinux.org/How_to_keep_the_GPU_(and_CPU)_cooler

# "fanboost"; default: 90 degrees C
class/drm/card0/device/hwmon/hwmon0/temp1_auto_point1_temp = 85000
# "downclock"; default: 95
class/drm/card0/device/hwmon/hwmon0/temp1_max = 83000
# "critical"; default: 105
class/drm/card0/device/hwmon/hwmon0/temp1_crit = 95000
# "emergency"; default: 135
# class/drm/card0/device/hwmon/hwmon0/temp1_emergency = 135000
