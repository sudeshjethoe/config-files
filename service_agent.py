#!/usr/bin/python

import numpy
import logging
import sys
import time
import os
import platform 
import subprocess
from IPython import embed
from socket import socket

## logger
LOG_FORMAT = "%(asctime)s   %(levelname)-7s   %(message)s"
LOG_FILE   = '/home/svjethoe/zenoss_api/python/zenoss-monitor.log'
LOGGER     = 'perfmon'
LOG_LEVEL  = 10 # debug == all
CARBON_SERVER = '127.0.0.1'
CARBON_PORT = 2003
########## FUNCTIONS #############
def set_logging(LOG_FORMAT,LOGFILE,LOGGER,LOG_LEVEL,console_output=False):
    ''' Set logging options and return logger '''
    #logging.basicConfig(format=LOG_FORMAT)
    logger = logging.getLogger(LOGGER)
    logger.setLevel(LOG_LEVEL)
    #### logging filehandle conf ###
    log_file_handle = logging.FileHandler ( LOG_FILE )
    logger.addHandler(log_file_handle)
    logger.propagate = console_output
    log_file_handle.setFormatter(logging.Formatter(LOG_FORMAT) )
    return logger

def get_loadavg():
    if platform.system() == "Linux":
        return open('/proc/loadavg').read().strip().split()[:3]
    else:   
        command = "uptime"
        process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        os.waitpid(process.pid, 0)
        output = process.stdout.read().replace(',', ' ').strip().split()
        length = len(output)
        return output[length - 3:length]

##########  MAIN   ###############
if __name__ == "__main__":
    log = set_logging(LOG_FORMAT,LOG_FILE,LOGGER,LOG_LEVEL)
    sock = socket()
    try:
        sock.connect( (CARBON_SERVER,CARBON_PORT) )
    except:
        print "Couldn't connect to %(server)s on port %(port)d, is carbon-agent.py running?" % { 'server':CARBON_SERVER, 'port':CARBON_PORT }
        sys.exit(1)

    date = time.time()-(30*24*60*60) # (1/1/2013)
    #date = time.time()-(24*60*60) # (1/1/2013)
    now   = int( date )
    #for i in xrange(300*24*60):
    for i in xrange(30*24*60):
        lines = []
        # report binom and normal data per round
        binom  = numpy.random.binomial(n=1, p=0.5) # network 90 % uptime
        #normal = numpy.random.normal(loc=190,scale=20) # application perf
        lines.append("system.binom %s %d" % (binom,now))
        #lines.append("system.normal %s %d" % (normal,now))
        message = '\n'.join(lines) + '\n' #all lines must end in a newline
        log.debug ("\n"+message)
        sock.sendall(message)
        time.sleep(0.0001)
        now  += 60 # report every minute
