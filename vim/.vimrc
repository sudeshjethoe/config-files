" default colorscheme desert
" color desert
set nocompatible
set number "":se nu! to toggle set number
syntax on
set mouse=a
set background=dark
set pastetoggle=<F2>
set bs=2 "backspace can delete!
"SEARCH OPTIONS "
set hlsearch "highlight search
set ignorecase "ignore case in search
set incsearch "incremental search
set magic "regex in search
" SPACING OPTIONS "
set smartindent
set autoindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
" FOLD OPTIONS "
" set foldmethod=indent
set foldmethod=syntax
set foldlevelstart=1
set foldnestmax=2
syntax on                           " syntax highlighing
" UNDO OPTIONS "
set undodir=~/.vim_runtime/undodir
set undofile "persistent undo
set autowrite " automatic write before compile / execute

" POWERLINE "
python from powerline.vim import setup as powerline_setup
python powerline_setup()
python del powerline_setup
set laststatus=2
set t_Co=256
