# Generator for normal distributed random data

import numpy
import logging

########## GLOBALS ###############
sys.stderr = sys.stdout
log = logging.getLogger('perfmon')

########## CLASSES ###############
class CustomGeneratorAPI():
    '''Initialize generator, returns random data when queried'''
    def __init__(self, debug=False, mean=200, stdev=20,max_items ):
        self.counter   = 0
        self.max_items = max_items

    def get_normal_status(self, mean, stdev):
        '''return random number distributed normally
        against mean and stdev'''
        if (self.counter >= self.max_items) : return False
        self.counter +=1
        return int(numpy.random.normal(loc=mean,scale=stdev))

    def get_binom_status(self, n=1, p):
        '''return binomial distributed random number'''
        return numpy.random.binomial(n,p)

